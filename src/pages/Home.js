import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { fetchHeroes } from '../redux/actions'

class Home extends PureComponent {
  componentDidMount() {
    fetchHeroes()
  }

  render() {
    return (
      <div className={'container'}>
        <div className={'row'}>
          <div className={'col-xs-12'}>
            <p>home</p>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { superHeroes } = state;
  return { superHeroes };
};

export default connect(mapStateToProps)(Home);