import * as types from '../actionTypes';
import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  superHeroes: [],
  loading: false
});

export default function (state = initialState, action) {
  switch (action.type) {
    case types.GET_SUPER_HEROES: {
      return state.set('superHeroes', action.superHeroes);
    }
    case types.SET_LOADING: {
      return state.set('loading', action.data);
    }
    default:
      return state;
  }
}
