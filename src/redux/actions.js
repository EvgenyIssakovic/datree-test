import * as types from './actionTypes';
import axios from 'axios';
import Immutable from 'immutable';

export const getSuperHeroes = () => ({
  type: types.GET_SUPER_HEROES,
  data: superHeroes
});

export const setLoading = loading => ({
  type: types.SET_LOADING,
  data: loading
});

export const fetchHeroes = () => dispatch => {
  dispatch(setUserLoading(true));
  axios.post('/api/super-hero/get-all')
    .then(res => {
      const superHeroes = res.data;
      if (user) {
        dispatch(getSuperHeroes(superHeroes))
      }
      dispatch(setUserLoading(false));
    });
};
