import React, { PureComponent } from 'react'
import { Route, withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Home from './pages/Home'
import { Layout, Spin } from 'antd'
import * as actions from './redux/actions'
import PageHeader from './components/PageHeader'

const { Content, Footer } = Layout

class App extends PureComponent {
  render () {
    const loading = this.props.user.get('loading')

    if (loading) {
      return (
        <div className={'loading-container'}>
          <Spin size="large"/>
        </div>
      )
    }

    return (
      <Layout className="layout">
        <PageHeader/>
        <Content style={{ padding: '0 50px' }}>
          <div style={{ background: '#fff', padding: 24, minHeight: 280, marginTop: 50 }}>
            <Route exact path="/" component={Home}/>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          React Node Starter
        </Footer>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  const { superHeroes } = state
  return { superHeroes }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
