const axios = require('axios')
const { stringify } = require('flatted');


module.exports = (router) => {
  router.get('/super-hero/get-all', async (req, res) => {
    axios.get('https://akabab.github.io/superhero-api/api/all.json')
      .then(function (response) {
        const { data } = response
        res.send(data)
      })
      .catch(function (error) {
        console.log(error);
      })
  });

  router.get('/super-hero/get', async (req, res) => {
    const { query } = req
    const url = `https://akabab.github.io/superhero-api/api/id/${query.id}.json`
    axios.get(url)
      .then(function (response) {
        const { data } = response
        res.send(data)
      })
      .catch(function (error) {
        console.log(error);
      })
  });
};
